const cacheFunction = require('../cacheFunction');

function callback(value){
    return value*100;
}

let output = cacheFunction(callback);

console.time("operation for first time");
console.log(output(100));
console.timeEnd("operation for first time");
console.time("operation with cache");
console.log(output(100));
console.timeEnd("operation with cache");