const limitFunctionCallCount = require('../limitFunctionCallCount');

let callback = () => {
    console.log("I am invoked");
}
let output = limitFunctionCallCount(callback,3);

output();
output();
output();
output();

let output2 = limitFunctionCallCount(callback,0);

output2();

let output3 = limitFunctionCallCount(callback);

output3();