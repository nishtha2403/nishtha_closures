function limitFunctionCallCount(cb, n) {
    let counter = 0;
    let invokeCb = () => {
        if(n == 0 || n == undefined){
            console.assert(!(n == 0 || n == undefined),"Limit not defined");
            return null;
        }

        if(counter<n){
            cb();
            counter++;
        }else{
            console.assert(false,"Limit reached!");
            return null;
        }
    }

    return invokeCb;
}

module.exports = limitFunctionCallCount;