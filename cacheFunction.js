function cacheFunction(cb) {
    let cacheObj = {};

    let invokeCb = (arg) => {
        if(cacheObj.hasOwnProperty(arg)){
            return cacheObj[arg];
        }
        cacheObj[arg] = cb(arg);
        return cacheObj[arg];
    }

    return invokeCb;
}

module.exports = cacheFunction;