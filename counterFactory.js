function counterFactory() {
    var counter = 0;
    let counterFactoryObj = {
        increment() {
            return ++counter;
        },
        decrement() {
            return --counter;
        }
    }

    return counterFactoryObj;
}

module.exports = counterFactory;
